using GameLibrary;
using Microsoft.Maui.Controls.Xaml;

namespace PilotBrothersSafe;

public partial class FirstLevelPage : ContentPage
{
    public Button[,] Buttons { get; set; }
    public Button NLButton { get; set; }

    public FirstLevelPage()
    {
        Buttons = GameViews.GetButtons(2, 2);
        Buttons[0, 0].BackgroundColor = Colors.Red;
        Buttons[0, 1].BackgroundColor = Colors.Red;

        Buttons[0, 0].Clicked += OnBut1Clicked;
        Buttons[0, 1].Clicked += OnBut2Clicked;
        Buttons[1, 0].Clicked += OnBut3Clicked;
        Buttons[1, 1].Clicked += OnBut4Clicked;

        Grid grid = GameViews.GetGameTable(Buttons);
        StackLayout sl = new StackLayout { Children = { grid } };
        Button levels = GameViews.GetLevelsButton();
        levels.Clicked += GoToMainPage;

        NLButton = GameViews.GetNextLevelButton();
        NLButton.Clicked += GoNextLevel;

        HorizontalStackLayout hsl = GameViews.GetHSL(levels, NLButton);
        Grid mainGrid = GameViews.GetMainGrid(sl, hsl);
        Content = new FlexLayout
        {
            Children = { mainGrid },
            HorizontalOptions = LayoutOptions.Center,
            VerticalOptions = LayoutOptions.Center,
            Padding = new Thickness(40)
        };

    }

    private void OnBut1Clicked(object sender, EventArgs e)
    {
        GameMethods.ChangeColors(Buttons, NLButton, 0, 0);
    }
    private void OnBut2Clicked(object sender, EventArgs e)
    {
        GameMethods.ChangeColors(Buttons, NLButton, 0, 1);
    }
    private void OnBut3Clicked(object sender, EventArgs e)
    {
        GameMethods.ChangeColors(Buttons, NLButton, 1, 0);
    }
    private void OnBut4Clicked(object sender, EventArgs e)
    {
        GameMethods.ChangeColors(Buttons, NLButton, 1, 1);
    }

    private async void GoNextLevel(object sender, EventArgs e)
    {
        await Shell.Current.GoToAsync(nameof(SecondLevelPage));
    }
    private async void GoToMainPage(object sender, EventArgs e)
    {
        await Shell.Current.GoToAsync("..");
    }

}
﻿namespace PilotBrothersSafe;

public partial class AppShell : Shell
{
	public AppShell()
	{
		InitializeComponent();
        Routing.RegisterRoute(nameof(FirstLevelPage), typeof(FirstLevelPage));
        Routing.RegisterRoute(nameof(SecondLevelPage), typeof(SecondLevelPage));
        Routing.RegisterRoute(nameof(ThirdLevelPage), typeof(ThirdLevelPage));

    }
}

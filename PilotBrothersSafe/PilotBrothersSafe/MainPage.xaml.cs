﻿namespace PilotBrothersSafe;

public partial class MainPage : ContentPage
{

	public MainPage()
	{
		InitializeComponent();
	}

	private async void GoToFirstLevel(object sender, EventArgs e)
	{
        await Shell.Current.GoToAsync(nameof(FirstLevelPage));
    }
    private async void GoSecondLevel(object sender, EventArgs e)
    {
        await Shell.Current.GoToAsync(nameof(SecondLevelPage));
    }
    private async void GoToThirdLevel(object sender, EventArgs e)
    {
       await  Shell.Current.GoToAsync(nameof(ThirdLevelPage));
    }

}


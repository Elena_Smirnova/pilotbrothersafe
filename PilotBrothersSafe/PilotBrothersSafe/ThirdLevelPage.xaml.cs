using GameLibrary;

namespace PilotBrothersSafe;

public partial class ThirdLevelPage : ContentPage
{
    public Button[,] Buttons { get; set; }
    public Button NLButton { get; set; }

    public ThirdLevelPage()
    {
        Buttons = GameViews.GetButtons(4, 4);
        Buttons[0, 1].BackgroundColor = Colors.Red;
        Buttons[0, 2].BackgroundColor = Colors.Red;
        Buttons[1, 2].BackgroundColor = Colors.Red;
        Buttons[2, 3].BackgroundColor = Colors.Red;
        Buttons[2, 2].BackgroundColor = Colors.Red;
        Buttons[3, 0].BackgroundColor = Colors.Red;
        Buttons[3, 3].BackgroundColor = Colors.Red;

        Buttons[0, 0].Clicked += OnBut1Clicked;
        Buttons[0, 1].Clicked += OnBut2Clicked;
        Buttons[0, 2].Clicked += OnBut3Clicked;
        Buttons[0, 3].Clicked += OnBut4Clicked;
        Buttons[1, 0].Clicked += OnBut5Clicked;
        Buttons[1, 1].Clicked += OnBut6Clicked;
        Buttons[1, 2].Clicked += OnBut7Clicked;
        Buttons[1, 3].Clicked += OnBut8Clicked;
        Buttons[2, 0].Clicked += OnBut9Clicked;
        Buttons[2, 1].Clicked += OnBut10Clicked;
        Buttons[2, 2].Clicked += OnBut11Clicked;
        Buttons[2, 3].Clicked += OnBut12Clicked;
        Buttons[3, 0].Clicked += OnBut13Clicked;
        Buttons[3, 1].Clicked += OnBut14Clicked;
        Buttons[3, 2].Clicked += OnBut15Clicked;
        Buttons[3, 3].Clicked += OnBut16Clicked;

        Grid grid = GameViews.GetGameTable(Buttons);
        StackLayout sl = new StackLayout { Children = { grid } };
        Button levels = GameViews.GetLevelsButton();
        levels.Clicked += GoToMainPage;

        NLButton = GameViews.GetNextLevelButton();

        HorizontalStackLayout hsl = GameViews.GetHSL(levels, NLButton);
        Grid mainGrid = GameViews.GetMainGrid(sl, hsl);
        Content = new FlexLayout
        {
            Children = { mainGrid },
            HorizontalOptions = LayoutOptions.Center,
            VerticalOptions = LayoutOptions.Center,
            Padding = new Thickness(40)
        };

    }

    private void OnBut1Clicked(object sender, EventArgs e)
    {
        GameMethods.ChangeColors(Buttons, NLButton, 0, 0);
    }
    private void OnBut2Clicked(object sender, EventArgs e)
    {
        GameMethods.ChangeColors(Buttons, NLButton, 0, 1);
    }
    private void OnBut3Clicked(object sender, EventArgs e)
    {
        GameMethods.ChangeColors(Buttons, NLButton, 0, 2);
    }
    private void OnBut4Clicked(object sender, EventArgs e)
    {
        GameMethods.ChangeColors(Buttons, NLButton, 0, 3);
    }
    private void OnBut5Clicked(object sender, EventArgs e)
    {
        GameMethods.ChangeColors(Buttons, NLButton, 1, 0);
    }
    private void OnBut6Clicked(object sender, EventArgs e)
    {
        GameMethods.ChangeColors(Buttons, NLButton, 1, 1);
    }
    private void OnBut7Clicked(object sender, EventArgs e)
    {
        GameMethods.ChangeColors(Buttons, NLButton, 1, 2);
    }
    private void OnBut8Clicked(object sender, EventArgs e)
    {
        GameMethods.ChangeColors(Buttons, NLButton, 1, 3);
    }
    private void OnBut9Clicked(object sender, EventArgs e)
    {
        GameMethods.ChangeColors(Buttons, NLButton, 2, 0);
    }
    private void OnBut10Clicked(object sender, EventArgs e)
    {
        GameMethods.ChangeColors(Buttons, NLButton, 2, 1);
    }
    private void OnBut11Clicked(object sender, EventArgs e)
    {
        GameMethods.ChangeColors(Buttons, NLButton, 2, 2);
    }
    private void OnBut12Clicked(object sender, EventArgs e)
    {
        GameMethods.ChangeColors(Buttons, NLButton, 2, 3);
    }
    private void OnBut13Clicked(object sender, EventArgs e)
    {
        GameMethods.ChangeColors(Buttons, NLButton, 3, 0);
    }
    private void OnBut14Clicked(object sender, EventArgs e)
    {
        GameMethods.ChangeColors(Buttons, NLButton, 3, 1);
    }
    private void OnBut15Clicked(object sender, EventArgs e)
    {
        GameMethods.ChangeColors(Buttons, NLButton, 3, 2);
    }
    private void OnBut16Clicked(object sender, EventArgs e)
    {
        GameMethods.ChangeColors(Buttons, NLButton, 3, 3);
    }

    private async void GoToMainPage(object sender, EventArgs e)
    {
        await Shell.Current.GoToAsync("..");
        await Shell.Current.GoToAsync("..");
        await Shell.Current.GoToAsync("..");
    }

}
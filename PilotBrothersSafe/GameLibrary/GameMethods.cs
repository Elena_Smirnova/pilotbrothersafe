﻿using Microsoft.Maui;

namespace GameLibrary
{
    public static class GameMethods
    {
        public static void ChangeColors(Button [,] buttons, Button nB , int fI, int sI)
        {
            ChangeColor(buttons[fI,sI]);
            for (int i = 0; i < buttons.GetLength(0); i++)
            {
                ChangeColor(buttons[fI, i]);
            }
            for (int i = 0; i < buttons.GetLength(1); i++)
            {
                ChangeColor(buttons[i, sI]);
            }

            var col = new List<Button>();
            foreach (Button button in buttons)
            {
                col.Add(button);
            }

            Check(col,nB);
        }

        private static void ChangeColor(Button button)
        {
            if (button.BackgroundColor == Colors.Red)
            {
                button.BackgroundColor = Colors.Yellow;
            }
            else if (button.BackgroundColor == Colors.Yellow)
            {
                button.BackgroundColor = Colors.Red;
            }
        }
        private static void Check(List<Button> c,Button nB)
        {
            if (c.All(b => b.BackgroundColor == Colors.Red))
            {
                Finish(c, nB);
            }
            else if(c.All(b => b.BackgroundColor == Colors.Yellow))
            {
                Finish(c, nB);
            }
        }
        private static void Finish(List<Button> buttons, Button nextButton)
        {

            foreach (var button in buttons)
            {
                button.ScaleTo(0, 400);
            }
            nextButton.BackgroundColor = Colors.Red;
            nextButton.IsEnabled = true;
        }
    }
}
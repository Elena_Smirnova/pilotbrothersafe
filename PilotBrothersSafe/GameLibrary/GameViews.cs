﻿namespace GameLibrary
{
    public static class GameViews
    {
        public static Button[,] GetButtons(int a, int b)
        {
            Button[,] buttons = new Button[a, b];
            for (int i = 0; i < buttons.GetLength(0); i++)
            {
                for (int y = 0; y < buttons.GetLength(1); y++)
                {
                    buttons[i, y] = new Button
                    {
                        BackgroundColor = Colors.Yellow,
                    };
                }
            }
            return buttons;
        }
        public static Grid GetGameTable(Button[,] buttons)
        {
            RowDefinitionCollection rowDefinitions = new RowDefinitionCollection();
            for (int i = 0; i < buttons.GetLength(0); i++)
            {
                rowDefinitions.Add(new RowDefinition());
            }
            ColumnDefinitionCollection columnDefinitions = new ColumnDefinitionCollection();
            for (int i = 0; i < buttons.GetLength(1); i++)
            {
                columnDefinitions.Add(new ColumnDefinition());
            }

            Grid grid = new Grid
            {
                HeightRequest = 300,
                WidthRequest = 300,
                VerticalOptions = LayoutOptions.Center,
                RowDefinitions = rowDefinitions,
                ColumnDefinitions = columnDefinitions
            };

            for (int i = 0; i < buttons.GetLength(0); i++)
            {
                for (int y = 0; y < buttons.GetLength(0); y++)
                {
                    grid.Add(buttons[i, y], i, y);

                }
            }

            return grid;
        }
        public static Button GetLevelsButton()
        {
            Button levels = new Button
            {
                BackgroundColor = Colors.Blue,
                Text = "Levels",
                WidthRequest = 120
            };
            return levels;
        }
        public static Button GetNextLevelButton()
        {
            Button nextLevel = new Button
            {
                BackgroundColor = Colors.Gray,
                Text = "Next Level",
                WidthRequest = 120,
                VerticalOptions = LayoutOptions.Center,
            };
            return nextLevel;
        }
        public static HorizontalStackLayout GetHSL(Button levels, Button nextLevel)
        {
            HorizontalStackLayout hsl = new HorizontalStackLayout
            {
                Children = { levels, nextLevel },
                Margin = new Thickness(30),
                HeightRequest = 40,
                VerticalOptions = LayoutOptions.Start,
            };
            return hsl;
        }
        public static Grid GetMainGrid(StackLayout sl, HorizontalStackLayout hsl)
        {
            Image image = new Image
            {
                Source = "cash.png",
                VerticalOptions = LayoutOptions.Center,
                HorizontalOptions = LayoutOptions.Center,
                Aspect = Aspect.Fill,
                Scale = 0.6
            };
            Label label = new Label
            {
                Text = "уровень пройден",
                VerticalOptions = LayoutOptions.End,
                HorizontalOptions = LayoutOptions.Center,
                FontSize = 20,
                TextColor = Colors.DarkRed,
                FontAttributes = FontAttributes.Bold
            };

            Grid mainGrid = new Grid
            {
                HeightRequest = 370,
                WidthRequest = 300,
                HorizontalOptions = LayoutOptions.Center,
                VerticalOptions = LayoutOptions.Center,
                RowDefinitions =
            {
                new RowDefinition(300),
                new RowDefinition(70)
            }
            };
            mainGrid.Add(image, 0, 0);
            mainGrid.Add(label, 0, 0);
            mainGrid.Add(sl, 0, 0);
            mainGrid.Add(hsl, 0, 1);
            return mainGrid;
        }

    }
}
